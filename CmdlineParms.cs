﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;
using CommandLine.Text;

namespace Sitecore.Support
{
    class CmdlineParms
    {
        [Option('c', "connection-string", Required = true, HelpText = "Connection string to the Sitecore Analytics server (cluster) hosting Mongo database which is to be processed")]
        public string ConnectionString { get; set; }

        [Option('d', "database", Required =true, HelpText = "Database name to be processed ( must be Sitecore Analytics database )")]
        public string DatabaseName { get; set; }
    }
}
