﻿namespace Sitecore.Support.Config
{
    using System.Configuration;
    using System.Text.RegularExpressions;
    class RegexFilterElement : ConfigurationElement
    {
        [ConfigurationProperty("regex", IsKey = true, IsRequired = true)]
        public string Regex
        {
            get { return (string)base["regex"]; }
            set { base["regex"] = value; }
        }
        /// <summary>
        /// Compiled regular expression ( from the 'Regex' property )
        /// </summary>
        public Regex Re
        {
            get; private set;
        }

        public void TryCompileRegex() {
            this.Re = new Regex(this.Regex, RegexOptions.Compiled | RegexOptions.IgnoreCase );
        }
    }
}
