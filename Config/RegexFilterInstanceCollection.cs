﻿namespace Sitecore.Support.Config
{
    using System;
    using System.Configuration;
    public class RegexFilterInstanceCollection: ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new RegexFilterElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((RegexFilterElement)element).Regex;
        }
    }
}
