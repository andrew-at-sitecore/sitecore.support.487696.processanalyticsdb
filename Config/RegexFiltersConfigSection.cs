﻿namespace Sitecore.Support.Config
{
    using System.Configuration;

    public class RegexFiltersConfigSection : ConfigurationSection
    {
        [ConfigurationProperty("", IsRequired =true, IsDefaultCollection =true)]
        public RegexFilterInstanceCollection Instances
        {
            get { return (RegexFilterInstanceCollection)this[""]; }
            set { this[""] = value; }
        }
    }
}
