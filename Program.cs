﻿namespace Sitecore.Support
{
    using System;
    using System.Collections.Generic;
    using NLog;
    using MongoDB.Bson;
    using MongoDB.Driver;
    using System.Configuration;
    using Sitecore.Support.Config;
    class Program
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static class Count
        {
            public static long Processed = 0;
            public static long Modified = 0;
            public static long Failed = 0;

            public static readonly long reportEveryX = 1000;
        }

        public static class Const
        {
            public static class Collections {
                public static readonly string TargetCollection = "Interactions";
            }
            public static class Fields
            {
                public static readonly string Pages = "Pages";
                public static readonly string Path = "Path";
                public static readonly string Url = "Url";
                public static readonly string QueryString = "QueryString";
            }
        }

        private static RegexFilterInstanceCollection RegexFilters { get; set; }
        static void Main(string[] args)
        {
            var cParams = new CmdlineParms();
            var isValid = CommandLine.Parser.Default.ParseArgumentsStrict(args, cParams);

            //Verifying regex filters configuration
            try
            {
                var cSection = ConfigurationManager.GetSection("regexFilters") as RegexFiltersConfigSection;
                RegexFilters = cSection.Instances;
                logger.Trace("Listing loaded regex filters:");
                foreach (RegexFilterElement rFilter in RegexFilters)
                {
                    rFilter.TryCompileRegex();
                    logger.Trace($"> '{rFilter.Regex}'");
                }
            } catch (Exception ex)
            {
                logger.Fatal(ex, "Failed to load regex filters configuration. Make sure to review the 'regexFilters' section in the 'App.config' file. Exiting");
                return;
            }

            IMongoClient mClient = null;
            //Attempt to connect to Mongo DB
            try
            {
                logger.Trace("Connecting to Mongo DB ...");
                mClient = new MongoClient(cParams.ConnectionString);
                logger.Trace("Connected.");
            } catch (Exception ex)
            {
                logger.Fatal(ex, "Failed to connect to Mongo DB");
                return;
            }

            //Get the analytics database and attempt to locate the 'Interactions' collection
            IMongoCollection<BsonDocument> targetCollection = null;
            try
            {
                logger.Trace($"Fetching '{cParams.DatabaseName}' database ...");
                var analyticsDb = mClient.GetDatabase(cParams.DatabaseName);
                logger.Trace("Done");
                logger.Trace("Fetching names of all available collections in the database ...");
                // Get all the collections in the database
                List<string> collectionNamesList = new List<string>();
                foreach (var bsonDoc in analyticsDb.ListCollectionsAsync().Result.ToListAsync().Result )
                {
                    collectionNamesList.Add(bsonDoc["name"].AsString);
                }
                logger.Trace($"Done (fetched {collectionNamesList.Count} collections)");
                logger.Trace($"Verifying '{Const.Collections.TargetCollection}' collection exists");
                if (! collectionNamesList.Contains(Const.Collections.TargetCollection))
                {
                    logger.Fatal($"Failed to find {Const.Collections.TargetCollection} in available collections list. Exiting");
                    logger.Debug("Listing discovered collections list:");
                    foreach (var cName in collectionNamesList)
                    {
                        logger.Debug($"> '{cName}'");
                    }
                    return;
                }
                logger.Trace("Done");
                logger.Trace($"Fetching '{Const.Collections.TargetCollection}' collection ...");
                targetCollection = analyticsDb.GetCollection<BsonDocument>(Const.Collections.TargetCollection);
                logger.Trace("Done");

            }
            catch (Exception ex)
            {
                logger.Fatal(ex, $"Failed to locate '{Const.Collections.TargetCollection}' collection in the '{cParams.DatabaseName}' database");
                return;
            }

            //Scan the 'Interactions' collection
            try
            {
                ProcessTargetCollection(targetCollection);
            }
            catch (Exception ex)
            {
                logger.Fatal(ex, "Failed to process target collection");
                return;
            }
        }

        private static void ProcessTargetCollection(IMongoCollection<BsonDocument> collection)
        {
            logger.Trace("Confirming user's intention");
            var confirmed = ConfirmUserIntention();
            if (confirmed)
            {
                logger.Trace("Confirmed");
            } else
            {
                logger.Trace("Declined. Exiting");
                return;
            }
            logger.Trace("Check log files for execution trace. Errors are not being logged to console");
            logger.Trace($"Obtaining cursor to iterate documents in the '{Const.Collections.TargetCollection}' collection");

            // db.getCollection('Interactions').find({"Pages" : { $elemMatch: { "Url.QueryString" : {$exists : true}}}})
            var hasUrlQueryString = Builders<BsonDocument>.Filter.Exists(_ => _["Url"]["QueryString"], true);
            var filter = Builders<BsonDocument>.Filter.ElemMatch( new StringFieldDefinition<BsonDocument>(Const.Fields.Pages), hasUrlQueryString);

            var projection = Builders<BsonDocument>.Projection.Include(Const.Fields.Pages);

            using (var cursor = collection.Find(filter).Project(projection).ToCursor()) {
                // Find only the documents matching criteria ( containing 'QueryString' data in 'Pages' collection )
                // Load only 'Pages' field from each matching document
                // Obtained cursor
                logger.Trace("Done");

                logger.Trace($"Iterating the collection documents");
                while (cursor.MoveNext())
                {
                    foreach (var document in cursor.Current)
                    {
                        try
                        {
                            ProcessInteractionDocument(document, collection);
                            Count.Processed += 1;
                            
                            //report status
                            if (Count.Processed % Count.reportEveryX == 0)
                            {
                                logger.Trace($"PROGRESS Processed:'{Count.Processed}'; Modified:'{Count.Modified}'; Failed: '{Count.Failed}'");
                            }
                        }
                        catch (Exception ex)
                        {
                            var dId = document["_id"].AsGuid;
                            logger.Error(ex, $"Failed to process document _id={dId}");
                            Count.Failed += 1;
                        }
                    }
                }
                // final status report
                logger.Trace($"PROGRESS FINAL Processed:'{Count.Processed}'; Modified:'{Count.Modified}'; Failed: '{Count.Failed}'");
            }
        }

        private static void ProcessInteractionDocument(BsonDocument document, IMongoCollection<BsonDocument> collection)
        {
            var documentId = document["_id"].AsGuid;
            logger.Debug($"Processing document '{documentId}'");
            var pagesSection = document[Const.Fields.Pages].AsBsonArray;
            var dataModified = false;
            for (var i=0; i<pagesSection.Count; i++)
            {
                logger.Debug($"Processing page #{i}");
                var page = pagesSection[i];
                try
                {
                    if (ProcessInteractionDocumentPage(page))
                    {
                        dataModified = true;
                    }
                } catch (Exception ex)
                {
                    logger.Error(ex, $"Failed to process page. Document id: {documentId}");
                }
            }

            if (dataModified)
            {
                logger.Debug($"Updating data for the document _id:'{documentId}'");
                var updateFilter = Builders<BsonDocument>.Filter.Eq("_id", documentId);
                var updateOperation = Builders<BsonDocument>.Update.Set(Const.Fields.Pages, pagesSection);
                var updateResult = collection.UpdateOneAsync(updateFilter, updateOperation).Result;

                var matchedCountNote = updateResult.MatchedCount == 1 ? "EXPECTED" : "UNEXPECTED";
                long modifiedCount = -1;
                var modifiedCountNote = "NA";
                if (updateResult.IsModifiedCountAvailable)
                {
                    modifiedCount = updateResult.ModifiedCount;
                    modifiedCountNote = updateResult.ModifiedCount == 1 ? "EXPECTED" : "UNEXPECTED";
                }
                logger.Debug($"Updated '{modifiedCount}' documents. Matched: '{matchedCountNote}'. Modified: '{modifiedCountNote}'");
                Count.Modified += 1;
            }
        }

        private static bool ProcessInteractionDocumentPage(BsonValue pageDocument)
        {
            var pageUrlPath = pageDocument[Const.Fields.Url][Const.Fields.Path].AsString;

            foreach (RegexFilterElement reFilter in RegexFilters)
            {
                if (reFilter.Re.IsMatch(pageUrlPath))
                {
                    //TODO: check QueryString exits
                    if (pageDocument[Const.Fields.Url].AsBsonDocument.Contains(Const.Fields.QueryString))
                    {
                        if (!String.IsNullOrEmpty(pageDocument[Const.Fields.Url][Const.Fields.QueryString].AsString)) { 
                            logger.Debug($"'{pageUrlPath}' matched '{reFilter.Regex}'. Resetting query string parameters.");
                            pageDocument[Const.Fields.Url][Const.Fields.QueryString] = "";
                            return true;
                        }
                    }
                    logger.Debug($"'{pageUrlPath}' matched '{reFilter.Regex}' but query string parameters are empty. Skipping.");
                }
            }
            return false;
        }

        private static bool ConfirmUserIntention()
        {
            var conclusive = false;
            var confirmed = false;

            do
            {
                Console.WriteLine("Proceed execution? (yes/no) [Type 'yes' to continue execution, 'no' to stop execution]");
                var answer = Console.ReadLine();
                if (answer.ToLower().Equals("yes"))
                {
                    confirmed = true;
                    conclusive = true;
                }
                else if (answer.ToLower().Equals("no"))
                {
                    confirmed = false;
                    conclusive = true;
                }
                else
                {
                    Console.WriteLine("Input not recognized. Must be 'yes' or 'no'");
                }
            } while (!conclusive);

            return confirmed;
        }
    }
}
